import { Grid, Typography } from '@mui/material';

export default function PageNotFound() {
  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
      sx={{ height: '75vh' }}>
      <Grid item>
        <Typography fontWeight="bold" variant="h2">
          404
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant="h4">That page could not be found...</Typography>
      </Grid>
    </Grid>
  );
}
