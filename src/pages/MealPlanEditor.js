import { Fragment, useState } from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import { Button, Grid, TextField } from '@mui/material';
import { createMealPlan, updateMealPlan } from '../graphql/mutations';
import MealPlanMasonry from '../components/common/MealPlanMasonry';
import PageToolbar from '../components/common/PageToolbar';
import Spinner from '../components/common/Spinner';
import ErrorDialog from '../components/dialogs/ErrorDialog';
import { MODES } from '../Modes';

export default function MealPlanEditor() {
  const navigate = useNavigate();
  const location = useLocation();
  const mode = location.pathname === '/edit-meal-plan' ? MODES.edit : 'create';
  const daysOfTheWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  const [mealPlan] = useState(
    mode === MODES.edit
      ? location.state.selectedMealPlan
      : {
          name: '',
          weekDays: daysOfTheWeek.map((weekDay) => ({
            name: weekDay,
            meals: [
              { name: 'Breakfast', food: [] },
              { name: 'Lunch', food: [] },
              { name: 'Dinner', food: [] },
              { name: 'Snacks', food: [] },
            ],
          })),
        }
  );
  const [invalidName, setInvalidName] = useState(false);
  const [nameExists, setNameExists] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [errorDialogOpen, setErrorDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const isValidInput = () => {
    var isValid = true;

    if (!mealPlan.name) {
      setInvalidName(true);
      isValid = false;
    }

    for (const plan of location.state.mealPlans) {
      if (plan.name.toLowerCase() === mealPlan.name.toLowerCase() && mealPlan !== plan) {
        setNameExists(true);
        isValid = false;
        break;
      }
    }

    return isValid;
  };

  const handleSaveMealPlan = async () => {
    if (!isValidInput()) {
      return;
    }

    setIsLoading(true);

    try {
      if (mode === MODES.edit) {
        await API.graphql(
          graphqlOperation(updateMealPlan, {
            input: { id: mealPlan.id, mealPlan: JSON.stringify(mealPlan) },
          })
        );
      } else {
        await API.graphql(
          graphqlOperation(createMealPlan, {
            input: { mealPlan: JSON.stringify(mealPlan) },
          })
        );
      }
      setIsLoading(false);
      navigate('/meal-plans', { replace: true })
    } catch (error) {
      setIsLoading(false);
      setErrorMessage('An error occurred. Failed to save meal plan.');
      setErrorDialogOpen(true);
    }
  };

  const handleNameChange = (e) => {
    mealPlan.name = e.target.value;
    setInvalidName(false);
    setNameExists(false);
  };

  const determineHelperText = () => {
    if (invalidName) {
      return 'Name is required';
    } else if (nameExists) {
      return 'Plan name already exists';
    } else {
      return '';
    }
  };

  return (
    <Fragment>
      {isLoading ? (
        <Spinner />
      ) : (
        <Fragment>
          <PageToolbar>
            <Grid
              container
              alignItems="center"
              justifyContent="space-between"
              rowSpacing={{ xs: 2 }}
              columnSpacing={{ md: 3 }}>
              <Grid item xs={12} md={3}>
                <TextField
                  autoFocus
                  required
                  error={invalidName || nameExists}
                  helperText={determineHelperText()}
                  onChange={(e) => handleNameChange(e)}
                  label="Meal Plan Name"
                  fullWidth
                  defaultValue={mode === MODES.edit ? mealPlan.name : ''}
                />
              </Grid>
              <Grid item xs={0} md={6} />
              <Grid item xs={6} md={1.5}>
                <Button onClick={handleSaveMealPlan} fullWidth variant="contained" color="primary">
                  Save Plan
                </Button>
              </Grid>
              <Grid item xs={5} md={1.5}>
                <Link to="/meal-plans" style={{ textDecoration: 'none' }}>
                  <Button fullWidth variant="contained" color="primary">
                    Cancel
                  </Button>
                </Link>
              </Grid>
            </Grid>
          </PageToolbar>
          <MealPlanMasonry mealPlan={mealPlan} mode={MODES.edit} />
          <ErrorDialog
            errorDialogOpen={errorDialogOpen}
            setErrorDialogOpen={setErrorDialogOpen}
            errorMessage={errorMessage}
          />
        </Fragment>
      )}
    </Fragment>
  );
}
