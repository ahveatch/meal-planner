import { Fragment, useEffect, useState } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from '@mui/material';
import { deleteFood } from '../graphql/mutations';
import { listFoods } from '../graphql/queries';
import FoodCatalogToolbar from '../components/FoodCatalogToolbar';
import FoodTable from '../components/FoodTable';
import Placeholder from '../components/common/Placeholder';
import Spinner from '../components/common/Spinner';
import ErrorDialog from '../components/dialogs/ErrorDialog';

export default function FoodCatalog() {
  const [foodRows, setFoodRows] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [selectedFood, setSelectedFood] = useState([]);
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [errorDialogOpen, setErrorDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    let isMounted = true;
    const fetchFood = async () => {
      try {
        const response = await API.graphql(graphqlOperation(listFoods));
        const items = response.data.listFoods.items;
        const fetchedFood = [];
        for (const item of items) {
          const food = JSON.parse(item.food);
          food.id = item.id;
          fetchedFood.push(food);
        }
        if (isMounted) setFoodRows(fetchedFood);
        if (isMounted) setIsLoading(false);
      } catch (error) {
        if (isMounted) setIsLoading(false);
        if (isMounted) setErrorMessage('An error occurred. Failed to retrieve food catalog.');
        if (isMounted) setErrorDialogOpen(true);
      }
    };

    fetchFood();
    return () => {
      isMounted = false;
    };
  }, []);

  const handleDeleteFood = async () => {
    setIsLoading(true);

    try {
      for (const food of selectedFood) {
        await API.graphql(graphqlOperation(deleteFood, { input: { id: food.id } }));
        const index = foodRows.indexOf(food);
        if (index !== -1) {
          foodRows.splice(index, 1);
        }
      }
      setSelectedFood([]);
      setSearchTerm('');
    } catch (error) {
      setErrorMessage('An error occurred. Failed to delete food.');
      setErrorDialogOpen(true);
    } finally {
      setIsLoading(false);
      setDeleteDialogOpen(false);
    }
  };

  const openDeleteDialog = () => {
    setDeleteDialogOpen(true);
  };

  const closeDeleteDialog = () => {
    setDeleteDialogOpen(false);
  };

  return (
    <Fragment>
      {isLoading ? (
        <Spinner />
      ) : (
        <Fragment>
          <FoodCatalogToolbar
            searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
            selected={selectedFood}
            handleClickDeleteFood={openDeleteDialog}
            searchDisabled={foodRows.length === 0}
          />
          {foodRows.length === 0 ? (
            <Placeholder>No Food Items</Placeholder>
          ) : (
            <FoodTable
              searchTerm={searchTerm}
              setSearchTerm={setSearchTerm}
              selected={selectedFood}
              setSelected={setSelectedFood}
              foodRows={foodRows}
            />
          )}
          <Dialog open={deleteDialogOpen} onClose={closeDeleteDialog}>
            <DialogTitle>Delete Food</DialogTitle>
            <DialogContent>
              <Typography>
                Are you sure you want to delete{' '}
                <Typography component={'span'} display="inline" fontWeight="bold">
                  {selectedFood.length}
                </Typography>{' '}
                selected food item(s)?
              </Typography>
            </DialogContent>
            <DialogActions>
              <Button variant="dialog" onClick={handleDeleteFood}>
                Delete
              </Button>
              <Button variant="dialog" onClick={closeDeleteDialog}>
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
          <ErrorDialog
            errorDialogOpen={errorDialogOpen}
            setErrorDialogOpen={setErrorDialogOpen}
            errorMessage={errorMessage}
          />
        </Fragment>
      )}
    </Fragment>
  );
}
