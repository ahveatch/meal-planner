import { Fragment, useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import { Button, Grid, TextField } from '@mui/material';
import { createFood, updateFood } from '../graphql/mutations';
import Spinner from '../components/common/Spinner';
import ErrorDialog from '../components/dialogs/ErrorDialog';
import { MODES } from '../Modes';

export default function FoodEditor() {
  const navigate = useNavigate();
  const location = useLocation();
  const mode = location.pathname === '/edit-food' ? MODES.edit : 'create';

  const [food] = useState(mode === MODES.edit ? location.state.selectedFood : {});
  const [invalidName, setInvalidName] = useState(false);
  const [invalidServingSizeAmount, setInvalidServingSizeAmount] = useState(false);
  const [invalidServingSizeUnit, setInvalidServingSizeUnit] = useState(false);
  const [invalidCalories, setInvalidCalories] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [errorDialogOpen, setErrorDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  const isValidInput = () => {
    var isValid = true;

    if (!food.name) {
      setInvalidName(true);
      isValid = false;
    }
    if (!food.servingSizeAmount || food.servingSizeAmount === 0) {
      setInvalidServingSizeAmount(true);
      isValid = false;
    }
    if (!food.servingSizeUnit) {
      setInvalidServingSizeUnit(true);
      isValid = false;
    }
    if (!food.calories) {
      setInvalidCalories(true);
      isValid = false;
    }

    return isValid;
  };

  const redirect = () => navigate('/food-catalog', { replace: true });

  const handleSaveFood = async () => {
    if (!isValidInput()) {
      return;
    }

    setIsLoading(true);

    try {
      if (mode === MODES.edit) {
        await API.graphql(
          graphqlOperation(updateFood, {
            input: { id: food.id, food: JSON.stringify(food) },
          })
        );
      } else {
        await API.graphql(
          graphqlOperation(createFood, {
            input: { food: JSON.stringify(food) },
          })
        );
      }
      setIsLoading(false);
      redirect();
    } catch (error) {
      setIsLoading(false);
      setErrorMessage('An error occurred. Failed to save food.');
      setErrorDialogOpen(true);
    }
  };

  const handleCancel = () => {
    redirect();
  };

  const handleFoodNameChange = (e) => {
    food.name = e.target.value;
    setInvalidName(false);
  };

  const handleCaloriesChange = (e) => {
    handleNutritionChange(e, 'calories');
    setInvalidCalories(false);
  };

  const handleServingSizeAmountChange = (e) => {
    handleNutritionChange(e, 'servingSizeAmount');
    setInvalidServingSizeAmount(false);
  };

  const handleServingSizeUnitChange = (e) => {
    food.servingSizeUnit = e.target.value;
    setInvalidServingSizeUnit(false);
  };

  const handleNutritionChange = (e, nutrition) => {
    e.target.value < 0
      ? (e.target.value = 0)
      : (food[nutrition] = Math.round(parseFloat(e.target.value) * 10) / 10);
  };

  return (
    <Fragment>
      {isLoading ? (
        <Spinner />
      ) : (
        <Fragment>
          <Grid
            container
            direction="column"
            alignItems="center"
            justifyContent="center"
            paddingBottom="20px">
            <Grid item sx={{ width: { sm: 500, xs: 250 } }}>
              <Grid container rowSpacing={{ xs: 1 }} justifyContent="space-between">
                <Grid item xs={12}>
                  <TextField
                    autoFocus
                    required
                    error={invalidName}
                    helperText={invalidName ? 'Name is required' : ''}
                    margin="dense"
                    label="Name"
                    fullWidth
                    defaultValue={mode === MODES.edit ? food.name : ''}
                    onChange={(e) => handleFoodNameChange(e)}
                  />
                </Grid>
                <Grid item xs={12} sm={5.8}>
                  <TextField
                    required
                    error={invalidServingSizeAmount}
                    helperText={invalidServingSizeAmount ? 'Serving Size Amount is required' : ''}
                    margin="dense"
                    label="Serving Size Amount"
                    type="number"
                    fullWidth
                    defaultValue={mode === MODES.edit ? food.servingSizeAmount : ''}
                    onChange={(e) => handleServingSizeAmountChange(e)}
                  />
                </Grid>
                <Grid item xs={12} sm={5.8}>
                  <TextField
                    required
                    error={invalidServingSizeUnit}
                    helperText={invalidServingSizeUnit ? 'Serving Size Unit is required' : ''}
                    margin="dense"
                    label="Serving Size Unit"
                    fullWidth
                    defaultValue={mode === MODES.edit ? food.servingSizeUnit : ''}
                    onChange={(e) => handleServingSizeUnitChange(e)}
                  />
                </Grid>
                <Grid item xs={5.8} sm={12}>
                  <TextField
                    required
                    error={invalidCalories}
                    helperText={invalidCalories ? 'Calories is required' : ''}
                    margin="dense"
                    label="Calories"
                    type="number"
                    fullWidth
                    defaultValue={mode === MODES.edit ? food.calories : ''}
                    onChange={(e) => handleCaloriesChange(e)}
                  />
                </Grid>
                <Grid item xs={5.8} sm={12}>
                  <TextField
                    margin="dense"
                    label="Fat"
                    type="number"
                    fullWidth
                    defaultValue={mode === MODES.edit ? food.fat : ''}
                    onChange={(e) => handleNutritionChange(e, 'fat')}
                  />
                </Grid>
                <Grid item xs={5.8} sm={12}>
                  <TextField
                    margin="dense"
                    label="Carbs"
                    type="number"
                    fullWidth
                    defaultValue={mode === MODES.edit ? food.carbs : ''}
                    onChange={(e) => handleNutritionChange(e, 'carbs')}
                  />
                </Grid>
                <Grid item xs={5.8} sm={12}>
                  <TextField
                    margin="dense"
                    label="Protein"
                    type="number"
                    fullWidth
                    defaultValue={mode === MODES.edit ? food.protein : ''}
                    onChange={(e) => handleNutritionChange(e, 'protein')}
                  />
                </Grid>
              </Grid>
              <Grid container justifyContent="space-around" paddingTop="16px">
                <Grid item xs={7}>
                  <Button fullWidth variant="contained" onClick={handleSaveFood}>
                    Save Food
                  </Button>
                </Grid>
                <Grid item xs={4}>
                  <Button fullWidth variant="contained" onClick={handleCancel}>
                    Cancel
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <ErrorDialog
            errorDialogOpen={errorDialogOpen}
            setErrorDialogOpen={setErrorDialogOpen}
            errorMessage={errorMessage}
          />
        </Fragment>
      )}
    </Fragment>
  );
}
