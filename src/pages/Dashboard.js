import { Fragment, useEffect, useState } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { ChevronLeft, ChevronRight } from '@mui/icons-material';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Typography,
  Backdrop,
  Tooltip,
} from '@mui/material';
import addDays from 'date-fns/addDays';
import addWeeks from 'date-fns/addWeeks';
import endOfWeek from 'date-fns/endOfWeek';
import format from 'date-fns/format';
import startOfWeek from 'date-fns/startOfWeek';
import subWeeks from 'date-fns/subWeeks';
import { getHistoryByStartDate, listMealPlans } from '../graphql/queries';
import { createHistory, updateHistory, deleteHistory } from '../graphql/mutations';
import DatePicker from '../components/DatePicker';
import MealPlanMasonry from '../components/common/MealPlanMasonry';
import MealPlanGenerator from '../components/MealPlanGenerator';
import PageToolbar from '../components/common/PageToolbar';
import Spinner from '../components/common/Spinner';
import ErrorDialog from '../components/dialogs/ErrorDialog';
import { MODES } from '../Modes';
import OptionsMenu from '../components/common/OptionsMenu';

export default function Dashboard() {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [selectedWeekDates, setSelectedWeekDates] = useState([]);
  const [mealPlan, setMealPlan] = useState();
  const [mealPlans, setMealPlans] = useState([]);
  const [history, setHistory] = useState();
  const [changeMealPlanDialogOpen, setChangeMealPlanDialogOpen] = useState(false);
  const [resetDialogOpen, setResetDialogOpen] = useState(false);
  const [newMealPlan, setNewMealPlan] = useState();
  const [selectedMealPlan, setSelectedMealPlan] = useState('');
  const [refreshState, setRefreshState] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [errorDialogOpen, setErrorDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [datePickerOpen, setDatePickerOpen] = useState(false);

  useEffect(() => {
    let isMounted = true;

    const buildEmptyMealPlan = () => {
      const newSelectedWeekDates = (() => {
        const weekDates = [];
        for (var i = 0; i < 7; i++) {
          weekDates.push(addDays(startOfWeek(selectedDate), i));
        }
        return weekDates;
      })();

      if (isMounted) setSelectedWeekDates(newSelectedWeekDates);

      if (isMounted) {
        setMealPlan({
          name: '',
          weekDays: newSelectedWeekDates.map((weekDate) => ({
            name: format(weekDate, 'EEE dd'),
            meals: [
              { name: 'Breakfast', food: [] },
              { name: 'Lunch', food: [] },
              { name: 'Dinner', food: [] },
              { name: 'Snacks', food: [] },
            ],
          })),
        });
      }
    };

    const fetchMealPlans = async () => {
      const fetchedMealPlans = [];
      const mealPlansResponse = await API.graphql(graphqlOperation(listMealPlans));
      const items = mealPlansResponse.data.listMealPlans.items;

      for (const item of items) {
        const mealPlan = JSON.parse(item.mealPlan);
        mealPlan.id = item.id;
        fetchedMealPlans.push(mealPlan);
      }
      if (isMounted) setMealPlans(fetchedMealPlans);
      return fetchedMealPlans;
    };

    const fetchHistory = async (fetchedMealPlans) => {
      let fetchedHistory = null;
      const historyResponse = await API.graphql({
        query: getHistoryByStartDate,
        variables: { startDate: format(startOfWeek(selectedDate), 'yyyy-MM-dd') },
      });
      fetchedHistory = historyResponse.data.getHistoryByStartDate.items[0];
      if (fetchedHistory) {
        var mealPlanExists = false;
        for (const mealPlan of fetchedMealPlans) {
          if (mealPlan.name === JSON.parse(fetchedHistory.mealPlan).name) {
            mealPlanExists = true;
          }
        }
        const historyMealPlan = JSON.parse(fetchedHistory.mealPlan);
        if (!mealPlanExists) {
          historyMealPlan.name = '';
        }
        if (isMounted) setHistory(fetchedHistory);
        if (isMounted) setMealPlan(historyMealPlan);
      } else {
        if (isMounted) setHistory();
      }
    };

    const fetchData = async () => {
      if (isMounted) setIsLoading(true);
      try {
        const fetchedMealPlans = await fetchMealPlans();
        await fetchHistory(fetchedMealPlans);
        if (isMounted) setIsLoading(false);
      } catch (error) {
        if (isMounted) setIsLoading(false);
        if (isMounted) setErrorMessage('An error occurred. Failed to retrieve user data.');
        if (isMounted) setErrorDialogOpen(true);
      }
    };

    buildEmptyMealPlan();
    fetchData();
    return () => {
      isMounted = false;
    };
  }, [selectedDate, refreshState]);

  const handleSelectMealPlan = async (event) => {
    setSelectedMealPlan(event.target.value);
  };

  const handleGenerateMealPlan = async (event) => {
    for (const mealPlan of mealPlans) {
      if (mealPlan.name === selectedMealPlan) {
        setSelectedMealPlan('');
        for (let i = 0; i < 7; i++) {
          mealPlan.weekDays[i].name = format(selectedWeekDates[i], 'EEE dd');
        }
        if (history) {
          setNewMealPlan(mealPlan);
          setChangeMealPlanDialogOpen(true);
        } else {
          setMealPlan(mealPlan);
          try {
            setIsLoading(true);
            const response = await API.graphql(
              graphqlOperation(createHistory, {
                input: {
                  startDate: format(startOfWeek(selectedDate), 'yyyy-MM-dd'),
                  mealPlan: JSON.stringify(mealPlan),
                },
              })
            );
            setHistory(response.data.createHistory);
            setIsLoading(false);
          } catch (error) {
            setIsLoading(false);
            setErrorMessage('An error occurred. Failed to save changes.');
            setErrorDialogOpen(true);
          }
        }
        break;
      }
    }
  };

  const handleChangeMealPlan = async () => {
    setIsLoading(true);
    setRefreshState(!refreshState);
    setMealPlan(newMealPlan);
    setChangeMealPlanDialogOpen(false);
    setSelectedMealPlan('');
    try {
      const response = await API.graphql(
        graphqlOperation(updateHistory, {
          input: { id: history.id, mealPlan: JSON.stringify(newMealPlan) },
        })
      );
      setHistory(response.data.updateHistory);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setErrorMessage('An error occurred. Failed to save changes.');
      setErrorDialogOpen(true);
    }
  };

  const handleDeleteHistory = async () => {
    if (history) {
      setIsLoading(true);
      try {
        await API.graphql(graphqlOperation(deleteHistory, { input: { id: history.id } }));
        setRefreshState(!refreshState);
        setHistory();
      } catch (error) {
        setErrorMessage('An error occurred. Failed to reset.');
        setErrorDialogOpen(true);
      } finally {
        setSelectedMealPlan('');
        setIsLoading(false);
        setResetDialogOpen(false);
      }
    } else {
      setResetDialogOpen(false);
    }
  };

  const handleToggle = (weekDayName, mealName, targetFood) => async () => {
    const updatedMealPlan = { ...mealPlan };
    const weekDayIndex = updatedMealPlan.weekDays.findIndex(
      (weekDay) => weekDay.name === weekDayName
    );
    const mealIndex = updatedMealPlan.weekDays[weekDayIndex].meals.findIndex(
      (meal) => meal.name === mealName
    );
    const foodIndex = updatedMealPlan.weekDays[weekDayIndex].meals[mealIndex].food.findIndex(
      (food) => food.name === targetFood.name
    );
    updatedMealPlan.weekDays[weekDayIndex].meals[mealIndex].food[foodIndex].completed =
      !targetFood.completed;
    setMealPlan(updatedMealPlan);

    try {
      API.graphql(
        graphqlOperation(updateHistory, {
          input: { id: history.id, mealPlan: JSON.stringify(updatedMealPlan) },
        })
      );
    } catch (error) {
      setErrorMessage('An error occurred. Failed to save changes.');
      setErrorDialogOpen(true);
    }
  };

  const handleParentToggle = async (weekDayName, mealName, checkedStatus) => {
    const updatedMealPlan = { ...mealPlan };

    const weekDayIndex = updatedMealPlan.weekDays.findIndex((wd) => wd.name === weekDayName);
    updatedMealPlan.weekDays[weekDayIndex].meals = updatedMealPlan.weekDays[weekDayIndex].meals.map(
      (m) => {
        if (m.name === mealName) {
          return {
            ...m,
            food: m.food.map((f) => ({
              ...f,
              completed: checkedStatus,
            })),
          };
        }
        return m;
      }
    );

    setMealPlan(updatedMealPlan);

    try {
      API.graphql(
        graphqlOperation(updateHistory, {
          input: { id: history.id, mealPlan: JSON.stringify(updatedMealPlan) },
        })
      );
    } catch (error) {
      setErrorMessage('An error occurred. Failed to save changes.');
      setErrorDialogOpen(true);
    }
  };

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const closeChangeMealPlanDialog = () => {
    setNewMealPlan();
    setChangeMealPlanDialogOpen(false);
  };

  const handleResetClicked = () => {
    setResetDialogOpen(true);
  };

  const closeResetDialog = () => {
    setResetDialogOpen(false);
  };

  const toggleDatePicker = () => {
    setDatePickerOpen((prevState) => !prevState);
  };

  return (
    <Fragment>
      {isLoading ? (
        <Spinner />
      ) : (
        <Fragment>
          <Backdrop style={{ zIndex: 10 }} open={datePickerOpen} />
          <Grid
            container
            alignItems="center"
            justifyContent="space-between"
            sx={{ paddingTop: '20px', paddingLeft: '20px', paddingRight: '20px' }}>
            <Grid sx={{ paddingRight: '10px' }} item xs={10} sm={6} md={4} lg={3}>
              <MealPlanGenerator
                selectedMealPlan={selectedMealPlan}
                mealPlans={mealPlans}
                handleSelectMealPlan={handleSelectMealPlan}
                handleGenerateMealPlan={handleGenerateMealPlan}
              />
            </Grid>
            <Grid sx={{ display: 'block' }}>
              <Tooltip title="Options">
                <OptionsMenu handleResetClicked={handleResetClicked}/>
              </Tooltip>
            </Grid>
          </Grid>
          <PageToolbar>
            <Grid container alignItems="center" justifyContent="center">
              <Grid item xs={12} md={4}>
                <Grid container alignItems="center" justifyContent="space-between">
                  <Grid item>
                    <IconButton
                      onClick={() => {
                        handleDateChange(subWeeks(startOfWeek(selectedDate), 1));
                      }}>
                      <ChevronLeft fontSize="large" />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <DatePicker
                      selectedDate={selectedDate}
                      handleDateChange={handleDateChange}
                      toggleDatePicker={toggleDatePicker}
                    />
                  </Grid>
                  <Grid item>
                    <IconButton
                      onClick={() => {
                        handleDateChange(addWeeks(startOfWeek(selectedDate), 1));
                      }}>
                      <ChevronRight fontSize="large" />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </PageToolbar>
          {mealPlan.weekDays && (
            <MealPlanMasonry
              mealPlan={mealPlan}
              mode={MODES.active}
              selectedDate={selectedDate}
              history={history}
              setHistory={setHistory}
              handleToggle={handleToggle}
              handleParentToggle={handleParentToggle}
            />
          )}
          <Dialog open={changeMealPlanDialogOpen} onClose={closeChangeMealPlanDialog}>
            <DialogTitle>Change Meal Plan</DialogTitle>
            <DialogContent>
              <Typography>
                Are you sure you want to change meal plans for{' '}
                <Typography component={'span'} display="inline" fontWeight="bold">
                  {format(startOfWeek(selectedDate), 'MMM dd') +
                    ' - ' +
                    format(endOfWeek(selectedDate), 'MMM dd')}
                </Typography>
                ? This will reset all meals for this week.
              </Typography>
            </DialogContent>
            <DialogActions>
              <Button variant="dialog" onClick={handleChangeMealPlan}>
                Confirm
              </Button>
              <Button variant="dialog" onClick={closeChangeMealPlanDialog}>
                Cancel
              </Button>
            </DialogActions>
          </Dialog>

          <Dialog open={resetDialogOpen} onClose={closeResetDialog}>
            <DialogTitle>Reset</DialogTitle>
            <DialogContent>
              <Typography>
                Are you sure you want to reset the week of{' '}
                <Typography component={'span'} display="inline" fontWeight="bold">
                  {format(startOfWeek(selectedDate), 'MMM dd') +
                    ' - ' +
                    format(endOfWeek(selectedDate), 'MMM dd')}
                </Typography>
                ?
              </Typography>
            </DialogContent>
            <DialogActions>
              <Button variant="dialog" onClick={handleDeleteHistory}>
                Reset
              </Button>
              <Button variant="dialog" onClick={closeResetDialog}>
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
          <ErrorDialog
            errorDialogOpen={errorDialogOpen}
            setErrorDialogOpen={setErrorDialogOpen}
            errorMessage={errorMessage}
          />
        </Fragment>
      )}
    </Fragment>
  );
}
