import { Fragment, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Tooltip,
  Typography,
} from '@mui/material';
import { deleteMealPlan } from '../graphql/mutations';
import { listMealPlans } from '../graphql/queries';
import MealPlanMasonry from '../components/common/MealPlanMasonry';
import MealPlanSelector from '../components/MealPlanSelector';
import PageToolbar from '../components/common/PageToolbar';
import Spinner from '../components/common/Spinner';
import Placeholder from '../components/common/Placeholder';
import ErrorDialog from '../components/dialogs/ErrorDialog';

export default function MealPlans() {
  const [mealPlans, setMealPlans] = useState([]);
  const [selectedMealPlan, setSelectedMealPlan] = useState({ name: '' });
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [errorDialogOpen, setErrorDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    let isMounted = true;
    const fetchMealPlans = async () => {
      try {
        const response = await API.graphql(graphqlOperation(listMealPlans));
        const items = response.data.listMealPlans.items;
        const fetchedMealPlans = [];
        for (const item of items) {
          const mealPlan = JSON.parse(item.mealPlan);
          mealPlan.id = item.id;
          fetchedMealPlans.push(mealPlan);
        }
        if (isMounted) setMealPlans(fetchedMealPlans);
        if (isMounted) setIsLoading(false);
      } catch (error) {
        if (isMounted) setIsLoading(false);
        if (isMounted) setErrorMessage('An error occurred. Failed to retrieve meal plans.');
        if (isMounted) setErrorDialogOpen(true);
      }
    };

    fetchMealPlans();
    return () => {
      isMounted = false;
    };
  }, []);

  const handleDeleteMealPlan = async () => {
    setIsLoading(true);
    try {
      await API.graphql(graphqlOperation(deleteMealPlan, { input: { id: selectedMealPlan.id } }));
      const index = mealPlans.indexOf(selectedMealPlan);
      if (index !== -1) {
        mealPlans.splice(index, 1);
      }
      setSelectedMealPlan({ name: '' });
    } catch (error) {
      setErrorMessage('An error occurred. Failed to delete meal plan.');
      setErrorDialogOpen(true);
    } finally {
      setIsLoading(false);
      setDeleteDialogOpen(false);
    }
  };

  const handleSelectMealPlan = (e) => {
    for (const mealPlan of mealPlans) {
      if (mealPlan.name === e.target.value) {
        setSelectedMealPlan(mealPlan);
      }
    }
  };

  const openDeleteDialog = () => {
    setDeleteDialogOpen(true);
  };

  const closeDeleteDialog = () => {
    setDeleteDialogOpen(false);
  };

  return (
    <Fragment>
      {isLoading ? (
        <Spinner />
      ) : (
        <Fragment>
          <Grid
            container
            alignItems="center"
            justifyContent="space-between"
            sx={{ padding: '20px' }}>
            <Grid item xs={12} sm={2.5} md={1.5}>
              <Link
                to="/create-meal-plan"
                state={{
                  mealPlans: mealPlans,
                }}
                sx={{ textDecoration: 'none' }}>
                <Button fullWidth variant="contained" color="primary">
                  Create Plan
                </Button>
              </Link>
            </Grid>
          </Grid>
          <PageToolbar>
            <Grid container alignItems="center" justifyContent="space-between">
              <Grid item xs={7} sm={5} md={3}>
                <MealPlanSelector
                  selectedMealPlan={selectedMealPlan}
                  mealPlans={mealPlans}
                  handleSelectMealPlan={handleSelectMealPlan}
                />
              </Grid>
              <Grid item xs={4.5} sm={3} md={2}>
                {selectedMealPlan.name !== '' && (
                  <Fragment>
                    <Link
                      to="/edit-meal-plan"
                      state={{
                        selectedMealPlan: selectedMealPlan,
                        mealPlans: mealPlans,
                      }}
                      style={{ textDecoration: 'none' }}>
                      <Tooltip title="Edit">
                        <IconButton>
                          <EditIcon fontSize="large" />
                        </IconButton>
                      </Tooltip>
                    </Link>
                    <Tooltip title="Delete">
                      <IconButton onClick={openDeleteDialog}>
                        <DeleteIcon fontSize="large" />
                      </IconButton>
                    </Tooltip>
                  </Fragment>
                )}
              </Grid>
              <Grid item xs={0} sm={1} md={4.5} />
            </Grid>
          </PageToolbar>
          {selectedMealPlan.name !== '' ? (
            <MealPlanMasonry mealPlan={selectedMealPlan} mode="readonly" />
          ) : (
            <Placeholder>Select a Meal Plan</Placeholder>
          )}
          <Dialog open={deleteDialogOpen} onClose={closeDeleteDialog}>
            <DialogTitle>Delete Meal Plan</DialogTitle>
            <DialogContent>
              <Typography>
                Are you sure you want to delete the{' '}
                <Typography component={'span'} display="inline" fontWeight="bold">
                  {selectedMealPlan.name}
                </Typography>{' '}
                meal plan?
              </Typography>
            </DialogContent>
            <DialogActions>
              <Button variant="dialog" onClick={handleDeleteMealPlan}>
                Delete
              </Button>
              <Button variant="dialog" onClick={closeDeleteDialog}>
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
          <ErrorDialog
            errorDialogOpen={errorDialogOpen}
            setErrorDialogOpen={setErrorDialogOpen}
            errorMessage={errorMessage}
          />
        </Fragment>
      )}
    </Fragment>
  );
}
