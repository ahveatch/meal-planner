import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { withAuthenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import { createTheme, ThemeProvider } from '@mui/material';
import Dashboard from './pages/Dashboard';
import FoodCatalog from './pages/FoodCatalog';
import FoodEditor from './pages/FoodEditor';
import MealPlanEditor from './pages/MealPlanEditor';
import MealPlans from './pages/MealPlans';
import PageNotFound from './pages/PageNotFound';
import { Header } from './components/AuthHeader';
import Navbar from './components/navigation/Navbar';

const customTheme = createTheme({
  palette: {
    primary: {
      main: '#ffa07a',
      contrastText: '#fff',
    },
    secondary: {
      main: '#fff',
    },
    text: {
      primary: '#000',
      disabled: '#000',
    },
  },
  components: {
    MuiAppBar: {
      styleOverrides: {
        root: {
          '& .MuiButton-root': {
            backgroundColor: '#ffa07a',
            '&:hover': {
              backgroundColor: '#f89770',
            },
          },
        },
      },
    },
    MuiTableRow: {
      styleOverrides: {
        root: {
          '&.Mui-selected, &.Mui-selected:hover': {
            backgroundColor: 'rgba(0, 0, 0, 0.04)',
          },
        },
      },
    },
    MuiMenuItem: {
      styleOverrides: {
        root: {
          '&.Mui-selected, &.Mui-selected:hover': {
            backgroundColor: 'rgba(0, 0, 0, 0.04)',
          },
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          background: '#757575',
          whiteSpace: 'nowrap',
          '&:hover': {
            backgroundColor: '#606060',
          },
        },
      },
      variants: [
        {
          props: { variant: 'dialog' },
          style: {
            backgroundColor: '#fff',
            color: '#ffa07a',
            '&:hover': {
              backgroundColor: '#fafafa',
            },
          },
        },
      ],
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          margin: '16px',
          width: 'calc(100% - 40px)',
          maxHeight: 'calc(100% - 40px)',
        },
      },
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {
          '&.Mui-checked, &.MuiCheckbox-indeterminate': {
            color: '#6EDB8F',
          },
        },
      },
    },
  },
});

function App({ signOut, user }) {
  return (
    <ThemeProvider theme={customTheme}>
      <Router>
        <Navbar signOut={signOut} user={user} />
        <Routes>
          <Route path="/" element={<Dashboard />} />
          <Route path="/meal-plans" element={<MealPlans />} />
          <Route path="/create-meal-plan" element={<MealPlanEditor />} />
          <Route path="/edit-meal-plan" element={<MealPlanEditor />} />
          <Route path="/food-catalog" element={<FoodCatalog />} />
          <Route path="/create-food" element={<FoodEditor />} />
          <Route path="/edit-food" element={<FoodEditor />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default withAuthenticator(App, {
  components: {
    Header,
  },
});
