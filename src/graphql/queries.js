/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getHistory = /* GraphQL */ `
  query GetHistory($id: ID!) {
    getHistory(id: $id) {
      id
      startDate
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listHistories = /* GraphQL */ `
  query ListHistories(
    $filter: ModelHistoryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listHistories(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        startDate
        mealPlan
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getMealPlan = /* GraphQL */ `
  query GetMealPlan($id: ID!) {
    getMealPlan(id: $id) {
      id
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listMealPlans = /* GraphQL */ `
  query ListMealPlans(
    $filter: ModelMealPlanFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listMealPlans(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        mealPlan
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getFood = /* GraphQL */ `
  query GetFood($id: ID!) {
    getFood(id: $id) {
      id
      food
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listFoods = /* GraphQL */ `
  query ListFoods(
    $filter: ModelFoodFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listFoods(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        food
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getHistoryByStartDate = /* GraphQL */ `
  query GetHistoryByStartDate(
    $startDate: AWSDate
    $sortDirection: ModelSortDirection
    $filter: ModelHistoryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    getHistoryByStartDate(
      startDate: $startDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        startDate
        mealPlan
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
