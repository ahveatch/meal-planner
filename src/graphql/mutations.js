/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createHistory = /* GraphQL */ `
  mutation CreateHistory(
    $input: CreateHistoryInput!
    $condition: ModelHistoryConditionInput
  ) {
    createHistory(input: $input, condition: $condition) {
      id
      startDate
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateHistory = /* GraphQL */ `
  mutation UpdateHistory(
    $input: UpdateHistoryInput!
    $condition: ModelHistoryConditionInput
  ) {
    updateHistory(input: $input, condition: $condition) {
      id
      startDate
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteHistory = /* GraphQL */ `
  mutation DeleteHistory(
    $input: DeleteHistoryInput!
    $condition: ModelHistoryConditionInput
  ) {
    deleteHistory(input: $input, condition: $condition) {
      id
      startDate
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createMealPlan = /* GraphQL */ `
  mutation CreateMealPlan(
    $input: CreateMealPlanInput!
    $condition: ModelMealPlanConditionInput
  ) {
    createMealPlan(input: $input, condition: $condition) {
      id
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateMealPlan = /* GraphQL */ `
  mutation UpdateMealPlan(
    $input: UpdateMealPlanInput!
    $condition: ModelMealPlanConditionInput
  ) {
    updateMealPlan(input: $input, condition: $condition) {
      id
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteMealPlan = /* GraphQL */ `
  mutation DeleteMealPlan(
    $input: DeleteMealPlanInput!
    $condition: ModelMealPlanConditionInput
  ) {
    deleteMealPlan(input: $input, condition: $condition) {
      id
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createFood = /* GraphQL */ `
  mutation CreateFood(
    $input: CreateFoodInput!
    $condition: ModelFoodConditionInput
  ) {
    createFood(input: $input, condition: $condition) {
      id
      food
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateFood = /* GraphQL */ `
  mutation UpdateFood(
    $input: UpdateFoodInput!
    $condition: ModelFoodConditionInput
  ) {
    updateFood(input: $input, condition: $condition) {
      id
      food
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteFood = /* GraphQL */ `
  mutation DeleteFood(
    $input: DeleteFoodInput!
    $condition: ModelFoodConditionInput
  ) {
    deleteFood(input: $input, condition: $condition) {
      id
      food
      createdAt
      updatedAt
      owner
    }
  }
`;
