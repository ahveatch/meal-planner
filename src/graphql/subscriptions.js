/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateHistory = /* GraphQL */ `
  subscription OnCreateHistory($owner: String) {
    onCreateHistory(owner: $owner) {
      id
      startDate
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateHistory = /* GraphQL */ `
  subscription OnUpdateHistory($owner: String) {
    onUpdateHistory(owner: $owner) {
      id
      startDate
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteHistory = /* GraphQL */ `
  subscription OnDeleteHistory($owner: String) {
    onDeleteHistory(owner: $owner) {
      id
      startDate
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateMealPlan = /* GraphQL */ `
  subscription OnCreateMealPlan($owner: String) {
    onCreateMealPlan(owner: $owner) {
      id
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateMealPlan = /* GraphQL */ `
  subscription OnUpdateMealPlan($owner: String) {
    onUpdateMealPlan(owner: $owner) {
      id
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteMealPlan = /* GraphQL */ `
  subscription OnDeleteMealPlan($owner: String) {
    onDeleteMealPlan(owner: $owner) {
      id
      mealPlan
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateFood = /* GraphQL */ `
  subscription OnCreateFood($owner: String) {
    onCreateFood(owner: $owner) {
      id
      food
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateFood = /* GraphQL */ `
  subscription OnUpdateFood($owner: String) {
    onUpdateFood(owner: $owner) {
      id
      food
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteFood = /* GraphQL */ `
  subscription OnDeleteFood($owner: String) {
    onDeleteFood(owner: $owner) {
      id
      food
      createdAt
      updatedAt
      owner
    }
  }
`;
