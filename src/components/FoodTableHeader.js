import { Checkbox, TableCell, TableHead, TableRow, TableSortLabel } from '@mui/material';

const headerCells = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Food',
  },
  {
    id: 'calories',
    numeric: true,
    disablePadding: false,
    label: 'Calories',
  },
  {
    id: 'fat',
    numeric: true,
    disablePadding: false,
    label: 'Fat (g)',
  },
  {
    id: 'carbs',
    numeric: true,
    disablePadding: false,
    label: 'Carbs (g)',
  },
  {
    id: 'protein',
    numeric: true,
    disablePadding: false,
    label: 'Protein (g)',
  },
];

export default function EnhancedTableHead(props) {
  const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;

  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
          />
        </TableCell>
        {headerCells.map((headerCell) => (
          <TableCell
            key={headerCell.id}
            align={headerCell.numeric ? 'right' : 'left'}
            padding={headerCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headerCell.id ? order : false}>
            <TableSortLabel
              active={orderBy === headerCell.id}
              direction={orderBy === headerCell.id ? order : 'asc'}
              onClick={createSortHandler(headerCell.id)}>
              {headerCell.label}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}
