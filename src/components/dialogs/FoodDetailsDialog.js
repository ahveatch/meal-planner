import { Fragment, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemText,
  TextField,
  Typography,
} from '@mui/material';
import { MODES } from '../../Modes';

export default function AddFoodDailog(props) {
  const {
    foodDialogOpen,
    setFoodDialogOpen,
    selectedFood,
    handleApply,
    handleRemove,
    setNumberOfServings,
    nutrition,
    mode,
  } = props;
  const [buttonDisabled, setButtonDisabled] = useState(true);

  const handleCloseFoodDialog = () => {
    setFoodDialogOpen(false);
    setButtonDisabled(true);
  };

  const handleNumberOfServingsChange = (e) => {
    e.target.value < 0 ? (e.target.value = 0) : setNumberOfServings(e.target.value);
    setButtonDisabled(false);
  };

  return (
    <Dialog
      sx={{ minHeight: '100px' }}
      fullWidth
      open={foodDialogOpen}
      onClose={handleCloseFoodDialog}>
      <DialogTitle>{selectedFood.name}</DialogTitle>
      <DialogContent dividers>
        <Grid container justifyContent="space-between">
          <Grid item xs={12}>
            {mode === MODES.edit || mode === MODES.active ? (
              <TextField
                margin="dense"
                label="Number of Servings"
                type="number"
                fullWidth
                defaultValue={selectedFood.numberOfServings}
                onChange={handleNumberOfServingsChange}
              />
            ) : (
              <TextField
                margin="dense"
                label="Number of Servings"
                type="number"
                fullWidth
                disabled
                defaultValue={selectedFood.numberOfServings}
              />
            )}
          </Grid>
          <Grid item xs={12}>
            <TextField
              margin="dense"
              label="Serving Size"
              fullWidth
              disabled
              defaultValue={selectedFood.servingSizeAmount + ' ' + selectedFood.servingSizeUnit}
            />
          </Grid>
        </Grid>
        <List>
          {nutrition.map((nutrition, index) => {
            return (
              <Fragment key={nutrition.label}>
                <ListItem
                  secondaryAction={
                    <IconButton edge="end" disabled>
                      <Typography sx={{ color: '#000' }}>
                        {selectedFood[nutrition.label.toLowerCase()] &&
                          Math.round(
                            selectedFood[nutrition.label.toLowerCase()] *
                              selectedFood.numberOfServings *
                              10
                          ) / 10}
                      </Typography>
                    </IconButton>
                  }>
                  <ListItemText primary={nutrition.label + (index !== 0 ? ' (g)' : '')} />
                </ListItem>
                {index + 1 !== nutrition.length && (
                  <Divider sx={{ marginLeft: 2, marginRight: 2 }} />
                )}
              </Fragment>
            );
          })}
        </List>
      </DialogContent>
      <DialogActions>
        {mode === MODES.readonly ? (
          <Button variant="dialog" onClick={handleCloseFoodDialog}>
            Ok
          </Button>
        ) : (
          <Fragment>
            <Button variant="dialog" onClick={handleApply} disabled={buttonDisabled}>
              Apply
            </Button>
            <Button variant="dialog" onClick={handleRemove}>
              Remove
            </Button>
            <Button variant="dialog" onClick={handleCloseFoodDialog}>
              Cancel
            </Button>
          </Fragment>
        )}
      </DialogActions>
    </Dialog>
  );
}
