import { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Search } from '@mui/icons-material';
import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  InputAdornment,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Paper,
  TextField,
  Typography,
} from '@mui/material';

export default function AddFoodDailog(props) {
  const {
    addFoodDialogOpen,
    setAddFoodDialogOpen,
    searchTerm,
    setSearchTerm,
    checked,
    setChecked,
    foodRows,
    handleSaveFood,
    servings,
  } = props;

  const handleCloseDialog = () => {
    setChecked([]);
    setSearchTerm('');
    setAddFoodDialogOpen(false);
  };

  const handleAddFoodToggle = (row) => () => {
    const selectedIndex = checked.indexOf(row);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(checked, row);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(checked.slice(1));
    } else if (selectedIndex === checked.length - 1) {
      newSelected = newSelected.concat(checked.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        checked.slice(0, selectedIndex),
        checked.slice(selectedIndex + 1)
      );
    }
    setChecked(newSelected);
  };

  const handleNumberOfServingsChange = (e, row) => {
    e.target.value < 0 ? (e.target.value = 0) : (servings[row.id] = e.target.value);
  };

  return (
    <Dialog
      sx={{ minHeight: '100px' }}
      fullWidth
      open={addFoodDialogOpen}
      PaperProps={{
        sx: {
          position: 'absolute',
          top: '0.5%',
          transformOrigin: 'top center',
        },
      }}
      onClose={handleCloseDialog}>
      <DialogTitle>Add Food</DialogTitle>
      <DialogContent>
        {foodRows.length === 0 ? (
          <Typography>
            The Food Catalog is empty. <Link to="/create-food">Create new food</Link>.
          </Typography>
        ) : (
          <Fragment>
            <Paper elevation={0} sx={{ position: 'sticky', top: 0, zIndex: 1 }}>
              <TextField
                label="Search"
                fullWidth
                margin="dense"
                onChange={(event) => {
                  setSearchTerm(event.target.value);
                }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <Search />
                    </InputAdornment>
                  ),
                }}
              />
            </Paper>
            <List>
              {foodRows
                .filter((val) => {
                  if (searchTerm === '') {
                    return val;
                  } else if (val.name.toLowerCase().includes(searchTerm.toLowerCase())) {
                    return val;
                  } else {
                    return false;
                  }
                })
                .map((row) => {
                  return (
                    <Box key={row.id}>
                      <ListItem key={row.id}>
                        {checked.indexOf(row) !== -1 && (
                          <TextField
                            sx={{ width: '30px' }}
                            edge="start"
                            size="small"
                            variant="standard"
                            type="number"
                            defaultValue="1"
                            onChange={(e) => handleNumberOfServingsChange(e, row)}
                          />
                        )}
                        <ListItemButton
                          sx={{ paddingRight: 0 }}
                          onClick={handleAddFoodToggle(row)}
                          dense>
                          <ListItemText primary={row.name} />
                          <ListItemIcon>
                            <Checkbox
                              edge="end"
                              checked={checked.indexOf(row) !== -1}
                              tabIndex={-1}
                              disableRipple
                            />
                          </ListItemIcon>
                        </ListItemButton>
                      </ListItem>
                      <Divider />
                    </Box>
                  );
                })}
            </List>
          </Fragment>
        )}
      </DialogContent>
      <DialogActions>
        <Button variant="dialog" onClick={handleSaveFood} disabled={foodRows.length === 0}>
          Add
        </Button>
        <Button variant="dialog" onClick={handleCloseDialog}>
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}
