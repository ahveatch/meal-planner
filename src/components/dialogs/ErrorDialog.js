import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@mui/material';

export default function ErrorDialog(props) {
  const { errorDialogOpen, setErrorDialogOpen, errorMessage } = props;

  const handleCloseErrorDialog = () => {
    setErrorDialogOpen(false);
  };

  return (
    <Dialog open={errorDialogOpen} onClose={handleCloseErrorDialog}>
      <DialogTitle>Error</DialogTitle>
      <DialogContent>
        <DialogContentText>{errorMessage}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant="dialog" onClick={handleCloseErrorDialog}>
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
}
