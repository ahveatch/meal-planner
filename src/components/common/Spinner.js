import { Box, CircularProgress } from '@mui/material';

export default function Spinner() {
  return (
    <Box sx={{ display: 'flex', height: '75vh', justifyContent: 'center', alignItems: 'center' }}>
      <CircularProgress />
    </Box>
  );
}
