import { Fragment, useEffect, useState, useRef } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import {
  Box,
  Button,
  Card,
  Checkbox,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Paper,
  styled,
  Typography,
  LinearProgress,
} from '@mui/material';
import { PieChart } from '@mui/x-charts/PieChart';
import { ChevronRight } from '@mui/icons-material';
import AddIcon from '@mui/icons-material/Add';
import CancelIcon from '@mui/icons-material/Cancel';
import { Masonry } from '@mui/lab';
import format from 'date-fns/format';
import startOfWeek from 'date-fns/startOfWeek';
import { listFoods } from '../../graphql/queries';
import { createHistory, updateHistory } from '../../graphql/mutations';
import AddFoodDialog from '../dialogs/AddFoodDialog';
import FoodDetailsDialog from '../dialogs/FoodDetailsDialog';
import ErrorDialog from '../dialogs/ErrorDialog';
import Spinner from './Spinner';
import { MODES } from '../../Modes';

const Item = styled(Paper)(({ theme }) => ({
  borderRadius: 0,
  border: '2px solid #e0e0e0',
  borderTop: 'none',
  padding: 10,
  backgroundColor: '#f5f5f5',
}));

export default function MealPlanMasonry(props) {
  const { mealPlan, mode, selectedDate, history, setHistory, handleToggle, handleParentToggle } =
    props;
  const [refreshState, setRefreshState] = useState(false);
  const [selectedMeal, setSelectedMeal] = useState({});
  const [selectedFood, setSelectedFood] = useState({});
  const [addFoodDialogOpen, setAddFoodDialogOpen] = useState(false);
  const [foodDialogOpen, setFoodDialogOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [checked, setChecked] = useState([]);
  const [foodRows, setFoodRows] = useState([]);
  const [servings, setServings] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [errorDialogOpen, setErrorDialogOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [numberOfServings, setNumberOfServings] = useState();
  const nutrition = [
    { label: 'Fat', color: '#6A55D6' },
    { label: 'Carbs', color: '#3ebede' },
    { label: 'Protein', color: '#FFB347' },
    { label: 'Calories', color: '#000' },
  ];
  const pieParams = { height: 150, margin: { right: 5 } };
  const palette = ['#6A55D6', '#3ebede', '#FFB347'];

  useEffect(() => {
    let isMounted = true;
    const fetchFood = async () => {
      try {
        const response = await API.graphql(graphqlOperation(listFoods));
        const items = response.data.listFoods.items;
        const fetchedFood = [];
        for (const item of items) {
          const food = JSON.parse(item.food);
          food.id = item.id;
          fetchedFood.push(food);
        }
        if (isMounted) setFoodRows(fetchedFood);
      } catch (error) {
        if (isMounted) setErrorMessage('An error occurred. Failed to retrieve food catalog.');
        if (isMounted) setErrorDialogOpen(true);
      }
    };

    fetchFood();
    return () => {
      isMounted = false;
    };
  }, []);

  const countWeekDayNutrition = (weekDay, nutrition) => {
    let nutritionCount = 0;
    for (const meal of weekDay.meals) {
      nutritionCount += countMealNutrition(meal, nutrition);
    }
    return Math.round(nutritionCount * 10) / 10;
  };

  const countMealNutrition = (meal, nutrition) => {
    let nutritionCount = 0;
    for (const food of meal.food) {
      if (food[nutrition.toLowerCase()]) {
        nutritionCount += food[nutrition.toLowerCase()] * food.numberOfServings;
      }
    }
    return nutritionCount;
  };

  const handleClickFood = (meal, food) => {
    setSelectedMeal(meal);
    setFoodDialogOpen(true);
    setSelectedFood(food);
  };

  const handleClickAddFood = (meal) => {
    setSelectedMeal(meal);
    setAddFoodDialogOpen(true);
  };

  const handleRemoveFood = (selectedMeal, selectedFood) => {
    const foodIndex = selectedMeal.food.indexOf(selectedFood);
    selectedMeal.food.splice(foodIndex, 1);
    setRefreshState(!refreshState);
  };

  const handleRemoveFoodAndSave = () => {
    const foodIndex = selectedMeal.food.indexOf(selectedFood);
    selectedMeal.food.splice(foodIndex, 1);

    if (mode === MODES.active) {
      try {
        API.graphql(
          graphqlOperation(updateHistory, {
            input: { id: history.id, mealPlan: JSON.stringify(mealPlan) },
          })
        );
      } catch (error) {
        setErrorMessage('An error occurred. Failed to save changes.');
        setErrorDialogOpen(true);
      }
    }
    setFoodDialogOpen(false);
  };

  const handleApply = () => {
    selectedFood.numberOfServings = numberOfServings;
    setFoodDialogOpen(false);
    if (mode === MODES.active) {
      try {
        API.graphql(
          graphqlOperation(updateHistory, {
            input: { id: history.id, mealPlan: JSON.stringify(mealPlan) },
          })
        );
      } catch (error) {
        setErrorMessage('An error occurred. Failed to save changes.');
        setErrorDialogOpen(true);
      }
    }
    setFoodDialogOpen(false);
  };

  const handleSaveFood = async () => {
    for (const checkedFood of JSON.parse(JSON.stringify(checked))) {
      checkedFood.numberOfServings = servings[checkedFood.id] || 1;
      selectedMeal.food.push(checkedFood);
    }

    if (mode === MODES.active) {
      try {
        if (history) {
          API.graphql(
            graphqlOperation(updateHistory, {
              input: { id: history.id, mealPlan: JSON.stringify(mealPlan) },
            })
          );
        } else {
          setIsLoading(true);
          const response = await API.graphql(
            graphqlOperation(createHistory, {
              input: {
                startDate: format(startOfWeek(selectedDate), 'yyyy-MM-dd'),
                mealPlan: JSON.stringify(mealPlan),
              },
            })
          );
          setHistory(response.data.createHistory);
          setIsLoading(false);
        }
      } catch (error) {
        setIsLoading(false);
        setErrorMessage('An error occurred. Failed to save changes.');
        setErrorDialogOpen(true);
      }
    }

    setAddFoodDialogOpen(false);
    setChecked([]);
    setServings([]);
    setSearchTerm('');
  };

  const determineParentCheckboxState = (meal) => {
    const completedCount = meal.food.filter((food) => food.completed).length;
    const isParentChecked = completedCount === meal.food.length;
    const isParentIndeterminate = completedCount > 0 && !isParentChecked;

    return { isParentChecked, isParentIndeterminate };
  };

  const determineWeekDayProgress = (weekDay) => {
    let completedCalories = 0;
    let totalCalories = 0;

    weekDay.meals.forEach((meal) => {
      meal.food.forEach((food) => {
        totalCalories += food.calories;

        if (food.completed) {
          completedCalories += food.calories;
        }
      });
    });

    if (totalCalories === 0) return 0;

    return (completedCalories / totalCalories) * 100;
  };

  const calculateWeekDayTotalMacros = (weekDay) => {
    let totalFat = 0;
    let totalCarbs = 0;
    let totalProtein = 0;

    weekDay.meals.forEach((meal) => {
      meal.food.forEach((food) => {
        totalFat += food.fat || 0;
        totalCarbs += food.carbs || 0;
        totalProtein += food.protein || 0;
      });
    });

    return {
      totalFat,
      totalCarbs,
      totalProtein,
    };
  };

  const calculateMacroPercentages = (fat, carbs, protein) => {
    const fatCalories = fat * 9;
    const carbsCalories = carbs * 4;
    const proteinCalories = protein * 4;

    const totalCalories = fatCalories + carbsCalories + proteinCalories;
    if (totalCalories === 0) {
      return {
        fat: 0,
        carbs: 0,
        protein: 0,
      };
    }

    return {
      fat: Math.round((fatCalories / totalCalories) * 100),
      carbs: Math.round((carbsCalories / totalCalories) * 100),
      protein: Math.round((proteinCalories / totalCalories) * 100),
    };
  };

  return (
    <Fragment>
      {isLoading ? (
        <Spinner />
      ) : (
        <Fragment>
          <Masonry columns={{ xs: 1, lg: 7 }} spacing={0}>
            {mealPlan.weekDays.map((weekDay) => {
              const { totalFat, totalCarbs, totalProtein } = calculateWeekDayTotalMacros(weekDay);
              const isNutritionDataAvailable = totalFat > 0 || totalCarbs > 0 || totalProtein > 0;
              const percentages = calculateMacroPercentages(totalFat, totalCarbs, totalProtein);

              return (
                <Item elevation={0} key={weekDay.name}>
                  <Card square sx={{ marginBottom: '10px' }}>
                    <Button
                      variant="contained"
                      fullWidth
                      sx={{
                        justifyContent: 'space-between',
                        textTransform: 'none',
                        borderRadius: '4px 4px 0 0',
                        backgroundColor: '#ffa07a',
                        padding: '10px 5px 10px 5px',
                        '&:hover': {
                          backgroundColor: '#f89770',
                        },
                      }}>
                      <Typography
                        color="secondary"
                        variant="h5"
                        sx={{
                          marginLeft: '10px',
                          marginTop: '2px',
                        }}>
                        {weekDay.name}
                      </Typography>
                      <ChevronRight fontSize="large" />
                    </Button>

                    <Box sx={{ marginTop: '10px' }} flexGrow={1}>
                      <PieChart
                        colors={isNutritionDataAvailable ? palette : ['#e0e0e0']}
                        legend={{ hidden: true }}
                        series={[
                          {
                            data: isNutritionDataAvailable
                              ? [
                                  { label: 'Fat', value: percentages.fat },
                                  { label: 'Carbs', value: percentages.carbs },
                                  { label: 'Protein', value: percentages.protein },
                                ]
                              : [{ value: 100 }],
                          },
                        ]}
                        {...pieParams}
                      />
                    </Box>
                    <List dense>
                      {nutrition.map((nutrition, index) => {
                        return (
                          <Fragment key={nutrition.label}>
                            <ListItem>
                              <Typography color={nutrition.color} fontWeight="bold">
                                {nutrition.label}:{' '}
                                <Typography component={'span'} display="inline">
                                  {countWeekDayNutrition(weekDay, nutrition.label)}{' '}
                                  {nutrition.label !== 'Calories' ? '(g)' : ''}
                                </Typography>
                              </Typography>
                            </ListItem>
                            {index + 1 !== nutrition.length && (
                              <Divider sx={{ marginLeft: 2, marginRight: 2 }} />
                            )}
                          </Fragment>
                        );
                      })}
                    </List>
                    <Card square>
                      {mode === MODES.active && (
                        <LinearProgress
                          sx={{
                            borderRadius: 5,
                            padding: '7px',
                            marginLeft: '10px',
                            marginRight: '10px',
                            marginBottom: '10px',
                            '& .MuiLinearProgress-barColorPrimary': {
                              backgroundColor: '#6EDB8F',
                            },
                            backgroundColor: '#e0e0e0',
                          }}
                          variant="determinate"
                          value={determineWeekDayProgress(weekDay)}
                        />
                      )}
                    </Card>
                  </Card>
                  {weekDay.meals.map((meal) => {
                    const { isParentChecked, isParentIndeterminate } =
                      determineParentCheckboxState(meal);
                    return (
                      <Card sx={{ marginBottom: '10px' }} square key={meal.name}>
                        <List dense>
                          <Box>
                            <ListItem>
                              <Box display="flex" alignItems="center" width="100%">
                                <ListItemText>
                                  <Typography variant="h6">{meal.name}</Typography>
                                </ListItemText>
                                {mode === MODES.active && meal.food.length > 0 && (
                                  <Checkbox
                                    edge="end"
                                    checked={isParentChecked}
                                    indeterminate={isParentIndeterminate}
                                    onChange={(event) =>
                                      handleParentToggle(
                                        weekDay.name,
                                        meal.name,
                                        event.target.checked
                                      )
                                    }
                                  />
                                )}
                              </Box>
                            </ListItem>
                          </Box>
                          <Divider />
                          {meal.food.map((food, index) => {
                            return (
                              <Box key={index}>
                                <ListItem
                                  secondaryAction={
                                    mode !== MODES.readonly &&
                                    (mode === MODES.active ? (
                                      <Checkbox
                                        edge="end"
                                        onChange={handleToggle(weekDay.name, meal.name, food)}
                                        checked={food.completed || false}
                                      />
                                    ) : (
                                      <IconButton
                                        edge="start"
                                        onClick={() => handleRemoveFood(meal, food)}>
                                        <CancelIcon />
                                      </IconButton>
                                    ))
                                  }
                                  disablePadding>
                                  <ListItemButton
                                    sx={{ paddingTop: '12px', paddingBottom: '12px' }}
                                    onClick={() => handleClickFood(meal, food)}>
                                    <ListItemText
                                      primary={
                                        food.name +
                                        (food.numberOfServings > 1
                                          ? ' (' + food.numberOfServings + ')'
                                          : '')
                                      }
                                    />
                                  </ListItemButton>
                                </ListItem>
                                <Divider />
                              </Box>
                            );
                          })}
                          <ListItem>
                            <Typography fontWeight="bold">
                              Calories:{' '}
                              <Typography component={'span'} display="inline">
                                {countMealNutrition(meal, 'Calories')}
                              </Typography>{' '}
                            </Typography>
                          </ListItem>
                          {mode !== MODES.readonly && (
                            <ListItem>
                              <Button
                                onClick={() => handleClickAddFood(meal)}
                                fullWidth
                                variant="contained">
                                <AddIcon fontSize="large" />
                              </Button>
                            </ListItem>
                          )}
                        </List>
                      </Card>
                    );
                  })}
                </Item>
              );
            })}
          </Masonry>
          <AddFoodDialog
            addFoodDialogOpen={addFoodDialogOpen}
            setAddFoodDialogOpen={setAddFoodDialogOpen}
            searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
            checked={checked}
            setChecked={setChecked}
            foodRows={foodRows}
            handleSaveFood={handleSaveFood}
            servings={servings}
          />
          <FoodDetailsDialog
            foodDialogOpen={foodDialogOpen}
            setFoodDialogOpen={setFoodDialogOpen}
            selectedFood={selectedFood}
            handleApply={handleApply}
            handleRemove={handleRemoveFoodAndSave}
            setNumberOfServings={setNumberOfServings}
            nutrition={nutrition}
            mode={mode}
          />
          <ErrorDialog
            errorDialogOpen={errorDialogOpen}
            setErrorDialogOpen={setErrorDialogOpen}
            errorMessage={errorMessage}
          />
        </Fragment>
      )}
    </Fragment>
  );
}
