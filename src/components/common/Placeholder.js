import { Box, Typography } from '@mui/material';

export default function Placeholder({ children }) {
  return (
    <Box sx={{ display: 'flex', height: '50vh', justifyContent: 'center', alignItems: 'center' }}>
      <Typography color="#9e9e9e" variant="h4">
        {children}
      </Typography>
    </Box>
  );
}
