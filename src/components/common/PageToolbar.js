import { Toolbar } from '@mui/material';

export default function PageToolbar({ children }) {
  return (
    <Toolbar
      sx={{
        paddingTop: '10px',
        paddingBottom: '10px',
        position: 'sticky',
        top: { xs: 56, md: 64 },
        backgroundColor: '#fff',
        zIndex: 3,
        borderBottom: '2px solid #e0e0e0'
      }}>
      {children}
    </Toolbar>
  );
}
