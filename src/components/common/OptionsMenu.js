import { useState } from 'react';
import {
  Menu,
  MenuList,
  MenuItem,
  ListItemText,
  ListItemIcon,
  IconButton,
  Fade,
  Divider,
} from '@mui/material';
import { Check, Delete, MoreVert } from '@mui/icons-material';

export default function OptionsMenu(props) {
  const { handleResetClicked } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedOption, setSelectedOption] = useState('Weekly View');

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton onClick={handleClick}>
        <MoreVert fontSize="large" />
      </IconButton>

      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        PaperProps={{
          style: {
            width: '220px',
          },
        }}
        TransitionComponent={Fade}>
        <MenuList>
          <MenuItem
            disableRipple
            onClick={() => {
              handleClose();
              setSelectedOption('Daily View');
            }}>
            {selectedOption === 'Daily View' && (
              <ListItemIcon>
                <Check />
              </ListItemIcon>
            )}
            <ListItemText inset={selectedOption !== 'Daily View'}>Daily View</ListItemText>
          </MenuItem>
          <MenuItem
            disableRipple
            onClick={() => {
              handleClose();
              setSelectedOption('Weekly View');
            }}>
            {selectedOption === 'Weekly View' && (
              <ListItemIcon>
                <Check />
              </ListItemIcon>
            )}
            <ListItemText inset={selectedOption !== 'Weekly View'}>Weekly View</ListItemText>
          </MenuItem>
          <Divider />
          <MenuItem
            disableRipple
            onClick={() => {
              handleClose();
              handleResetClicked();
            }}>
            <ListItemIcon fontSize="large">
              <Delete />
            </ListItemIcon>
            <ListItemText>Clear Meals</ListItemText>
          </MenuItem>
        </MenuList>
      </Menu>
    </div>
  );
}
