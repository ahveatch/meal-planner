import { Box, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { PickersDay, DesktopDatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import endOfWeek from 'date-fns/endOfWeek';
import format from 'date-fns/format';
import isSameDay from 'date-fns/isSameDay';
import isWithinInterval from 'date-fns/isWithinInterval';
import startOfWeek from 'date-fns/startOfWeek';

const CustomPickersDay = styled(PickersDay, {
  shouldForwardProp: (prop) =>
    prop !== 'dayIsBetween' && prop !== 'isFirstDay' && prop !== 'isLastDay',
})(({ theme, dayIsBetween, isFirstDay, isLastDay }) => ({
  ...(dayIsBetween && {
    borderRadius: 0,
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    '&:hover, &:focus': {
      backgroundColor: '#f89770',
    },
  }),
  ...(isFirstDay && {
    borderTopLeftRadius: '50%',
    borderBottomLeftRadius: '50%',
  }),
  ...(isLastDay && {
    borderTopRightRadius: '50%',
    borderBottomRightRadius: '50%',
  }),
}));

export default function DatePicker(props) {
  const { selectedDate, handleDateChange, toggleDatePicker } = props;

  const renderWeekPickerDay = (date, selectedDates, pickersDayProps) => {
    if (!selectedDate) {
      return <PickersDay {...pickersDayProps} />;
    }

    const start = startOfWeek(selectedDate);
    const end = endOfWeek(selectedDate);

    const dayIsBetween = isWithinInterval(date, { start, end });
    const isFirstDay = isSameDay(date, start);
    const isLastDay = isSameDay(date, end);

    return (
      <CustomPickersDay
        {...pickersDayProps}
        dayIsBetween={dayIsBetween}
        isFirstDay={isFirstDay}
        isLastDay={isLastDay}
      />
    );
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DesktopDatePicker
        showDaysOutsideCurrentMonth
        renderDay={renderWeekPickerDay}
        value={selectedDate}
        onChange={(newDate) => {
          handleDateChange(newDate);
        }}
        onOpen={() => {
          toggleDatePicker();
        }}
        onClose={() => {
          toggleDatePicker();
        }}
        renderInput={({ inputRef, InputProps }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
            }}>
            <Typography color="#606060" fontWeight="bold" ref={inputRef}>
              {format(startOfWeek(selectedDate), 'MMM dd') +
                ' - ' +
                format(endOfWeek(selectedDate), 'MMM dd')}
            </Typography>
            {InputProps?.endAdornment}
          </Box>
        )}
      />
    </LocalizationProvider>
  );
}
