import { useState } from 'react';
import { Checkbox, Table, TableBody, TableCell, TableContainer, TableRow } from '@mui/material';
import FoodTableHeader from './FoodTableHeader';

export default function FoodTable(props) {
  const { searchTerm, setSearchTerm, selected, setSelected, foodRows } = props;
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('calories');

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  };

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = foodRows.map((n) => n);
      setSelected(newSelecteds);
      setSearchTerm('');
      return;
    }
    setSelected([]);
  };

  const handleClick = (row) => {
    const selectedIndex = selected.indexOf(row);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, row);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const isSelected = (row) => selected.indexOf(row) !== -1;

  return (
    <TableContainer>
      <Table sx={{ minWidth: 750 }} size={'medium'} stickyHeader>
        <FoodTableHeader
          numSelected={selected.length}
          order={order}
          orderBy={orderBy}
          onSelectAllClick={handleSelectAllClick}
          onRequestSort={handleRequestSort}
          rowCount={foodRows.length}
        />
        <TableBody>
          {foodRows
            .filter((food) => {
              if (searchTerm === '') {
                return food;
              } else if (food.name.toLowerCase().includes(searchTerm.toLowerCase())) {
                return food;
              } else {
                return false;
              }
            })
            .slice()
            .sort(getComparator(order, orderBy))
            .map((food) => {
              const isItemSelected = isSelected(food);

              return (
                <TableRow
                  hover
                  onClick={() => handleClick(food)}
                  key={food.id}
                  selected={isItemSelected}>
                  <TableCell padding="checkbox">
                    <Checkbox checked={isItemSelected} />
                  </TableCell>
                  <TableCell component="th" scope="row" padding="none">
                    {food.name}
                  </TableCell>
                  <TableCell align="right">{food.calories}</TableCell>
                  <TableCell align="right">{food.fat}</TableCell>
                  <TableCell align="right">{food.carbs}</TableCell>
                  <TableCell align="right">{food.protein}</TableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
