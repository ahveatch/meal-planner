import { Fragment } from 'react';
import { Link } from 'react-router-dom';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { Search } from '@mui/icons-material';
import { Button, Grid, InputAdornment, TextField, Typography } from '@mui/material';
import PageToolbar from './common/PageToolbar';

export default function FoodTableToolbar(props) {
  const { searchTerm, setSearchTerm, selected, handleClickDeleteFood, searchDisabled } = props;

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
  };

  return (
    <PageToolbar>
      {selected.length > 0 ? (
        <Fragment>
          <Typography sx={{ flex: '1 1 100%' }}>{selected.length} selected</Typography>
          {selected.length === 1 && (
            <Link
              to="/edit-food"
              state={{
                selectedFood: selected[0],
              }}
              style={{ textDecoration: 'none' }}>
              <Tooltip title="Edit">
                <IconButton>
                  <EditIcon fontSize="large" />
                </IconButton>
              </Tooltip>
            </Link>
          )}
          <Tooltip title="Delete">
            <IconButton onClick={handleClickDeleteFood}>
              <DeleteIcon fontSize="large" />
            </IconButton>
          </Tooltip>
        </Fragment>
      ) : (
        <Grid container justifyContent="space-between" alignItems="center" rowSpacing={{ xs: 2 }}>
          <Grid item xs={12} sm={2.5} md={1.5}>
            <Link to="/create-food" style={{ textDecoration: 'none' }}>
              <Button fullWidth variant="contained">
                Create Food
              </Button>
            </Link>
          </Grid>
          <Grid item xs={12} sm={5} md={3}>
            <TextField
              disabled={searchDisabled}
              label="Search"
              fullWidth
              value={searchTerm}
              onChange={(e) => {
                handleSearchChange(e);
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Search />
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
        </Grid>
      )}
    </PageToolbar>
  );
}
