import { Fragment, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {
  AppBar,
  Box,
  Button,
  Container,
  IconButton,
  Menu,
  MenuItem,
  ListItem,
  Toolbar,
  Tooltip,
  Typography,
} from '@mui/material';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MenuIcon from '@mui/icons-material/Menu';
import RamenDiningIcon from '@mui/icons-material/RamenDining';
import MenuDrawer from './MenuDrawer';

const pages = [
  { name: 'Dashboard', path: '/' },
  { name: 'Meal Plans', path: '/meal-plans' },
  { name: 'Food Catalog', path: '/food-catalog' },
];
const settings = ['Sign out'];

const Navbar = (props) => {
  const { signOut, user } = props;
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [drawerState, setDrawerState] = useState(false);
  const navigate = useNavigate();

  const toggleDrawer = (open) => () => {
    setDrawerState(open);
  };

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleSignOut = () => {
    signOut();
    navigate('/', { replace: true })
  };

  return (
    <Fragment>
      <MenuDrawer toggleDrawer={toggleDrawer} drawerState={drawerState} pages={pages} />
      <AppBar sx={{ height: { xs: '56px', sm: '56px', md: '64px', lg: '64px' } }} position="sticky">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <RamenDiningIcon sx={{ display: { xs: 'none', md: 'flex', marginRight: '8px' } }} />
            <Typography
              variant="h5"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              Miso
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton size="large" onClick={toggleDrawer(true)} color="inherit">
                <MenuIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: 'block', md: 'none' },
                }}>
                {pages.map((page) => (
                  <MenuItem key={page.name} onClick={handleCloseNavMenu}>
                    <Typography textAlign="center">{page.name}</Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
            <RamenDiningIcon sx={{ display: { xs: 'flex', md: 'none', marginRight: '8px' } }} />
            <Typography
              variant="h5"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              Miso
            </Typography>
            <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
              {pages.map((page) => (
                <Link key={page.name} to={page.path} style={{ textDecoration: 'none' }}>
                  <Button
                    variant="contained"
                    disableElevation
                    onClick={handleCloseNavMenu}
                    sx={{ borderRadius: 0, height: 64 }}>
                    {page.name}
                  </Button>
                </Link>
              ))}
            </Box>

            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu}>
                <AccountCircle color="secondary" fontSize="large" />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}>
              <ListItem>
                <Typography fontWeight="bold">{user.username}</Typography>
              </ListItem>
              {settings.map((setting) => (
                <MenuItem key={setting} onClick={handleSignOut}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Toolbar>
        </Container>
      </AppBar>
    </Fragment>
  );
};
export default Navbar;
