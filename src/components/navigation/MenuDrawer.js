import { Link } from 'react-router-dom';
import { Box, Drawer, List, ListItem, ListItemText, ListItemIcon } from '@mui/material';
import FactCheckIcon from '@mui/icons-material/FactCheck';
import TableChartIcon from '@mui/icons-material/TableChart';
import ListAltIcon from '@mui/icons-material/ListAlt';

export default function MenuDrawer(props) {
  const { toggleDrawer, drawerState, pages } = props;

  return (
    <Drawer anchor="left" open={drawerState} onClose={toggleDrawer(false)}>
      <Box sx={{ width: 250 }} role="presentation" onClick={toggleDrawer(false)}>
        <List>
          {pages.map((page, index) => (
            <Link to={page.path} style={{ textDecoration: 'none', color: '#000' }} key={page.name}>
              <ListItem button>
                <ListItemIcon>
                  {index === 0 && <TableChartIcon />}
                  {index === 1 && <FactCheckIcon />}
                  {index === 2 && <ListAltIcon />}
                </ListItemIcon>
                <ListItemText color="#000" primary={page.name} />
              </ListItem>
            </Link>
          ))}
        </List>
      </Box>
    </Drawer>
  );
}
