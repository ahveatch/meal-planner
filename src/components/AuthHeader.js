import { Box, Typography } from '@mui/material';

export function Header() {
  return (
    <Box
      sx={{
        padding: '15px',
        backgroundColor: '#ffa07a',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
      }}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
        }}>
        <Typography sx={{ color: '#fff', fontWeight: 'bold' }} variant="h3">
          Miso
        </Typography>
        <Box
          component="img"
          src="/miso-logo-transparent.png"
          alt="Miso Icon"
          sx={{ width: '50px', marginLeft: '15px' }}
        />
      </Box>
      <Typography sx={{ color: '#fff', fontWeight: 'bold' }} variant="h3">
        Meal Planner
      </Typography>
    </Box>
  );
}
