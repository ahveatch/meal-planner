import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';

export default function MealPlanSelector(props) {
  const { selectedMealPlan, mealPlans, handleSelectMealPlan } = props;
  return (
    <FormControl sx={{ flex: 3 }} fullWidth size="small">
      <InputLabel>Meal Plan</InputLabel>
      <Select value={selectedMealPlan.name} label="Meal Plan" onChange={handleSelectMealPlan}>
        {mealPlans.length !== 0 ? (
          mealPlans.map((mealPlan) => {
            return (
              <MenuItem key={mealPlan.id} value={mealPlan.name}>
                {mealPlan.name}
              </MenuItem>
            );
          })
        ) : (
          <MenuItem value="">
            <em>No Meal Plans</em>
          </MenuItem>
        )}
      </Select>
    </FormControl>
  );
}
