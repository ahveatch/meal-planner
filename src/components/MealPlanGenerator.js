import { Button, ButtonGroup } from '@mui/material';
import MealPlanSelector from './MealPlanSelector';

export default function MealPlanGenerator(props) {
  const { selectedMealPlan, mealPlans, handleSelectMealPlan, handleGenerateMealPlan } = props;

  return (
    <ButtonGroup fullWidth>
      <MealPlanSelector
        selectedMealPlan={selectedMealPlan}
        mealPlans={mealPlans}
        handleSelectMealPlan={handleSelectMealPlan}
      />
      <Button disabled={!selectedMealPlan} onClick={handleGenerateMealPlan} sx={{ flex: 1 }} variant="contained">
        Generate
      </Button>
    </ButtonGroup>
  );
}
