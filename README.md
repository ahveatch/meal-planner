# Demo

To view the app using a demo account please visit https://www.my-meal-planner.com/ and login with the following credentials:

**Username:** demo  
**Password:** demo1234

# Pages and Features

### Login

Authentication page created with AWS Amplify.

![Alt text](/screenshots/login.png?raw=true)

### Dashboard

View and track meals for selected week on the Dashboard page with calculated nutritional values.

![Alt text](/screenshots/dashboard.png?raw=true)

### Meal Plans

Create or edit meal plans.

![Alt text](/screenshots/create-mealplan.png?raw=true)

![Alt text](/screenshots/edit-mealplan.png?raw=true)

### Food Catalog

Search or add new food using the Food Catalog.

![Alt text](/screenshots/food-catalog.png?raw=true)


### Create Food

Create food with input validation.

![Alt text](/screenshots/add-food.png?raw=true)


### Responsive Layout

![Alt text](/screenshots/responsive.png?raw=true)
